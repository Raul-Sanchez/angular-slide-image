// web.js
var express = require("express");
var logfmt = require("logfmt");
var engines = require('consolidate');
var app = express();

app.use(logfmt.requestLogger());

app.set('views', __dirname + '/views');
app.engine('html', engines.mustache);
app.set('view engine', 'html');
app.use("/images", express.static(__dirname + '/images'));
app.use("/libs", express.static(__dirname + '/images'));
app.use("/scripts", express.static(__dirname + '/images'));
app.use("/styles", express.static(__dirname + '/images'));
app.use("/views", express.static(__dirname + '/images'));
app.use("/", express.static(__dirname));


app.get('/angularSlider', function(req, res) {
	res.sendFile("views/angularSlider.html", {'root': './'});
});
app.get('/', function(req, res) {
	res.sendFile("views/index.html", {'root': './'});
});
var port = Number(process.env.PORT || 5000);
app.listen(port, function() {
  console.log("Listening on " + port);
});