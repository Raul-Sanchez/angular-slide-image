angular.module('sliderdemo')
.directive('angularSlider', ['$document', function($document) {	
    return{
    	restrict: 'E',
    	template: '<div class="slider" ng-style="sliderStyle"><div class="viewer" ng-style="viewerStyle"><div class="scroll" ng-style="viewerScrollStyle" state="loading"><span class="box" ng-repeat="image in images" ng-style="viewerBoxStyle"><span class="helper"></span><img  ng-src="{{image.url}}"></span></div><div class="carousel-control right arrow right-arrow" href="#"><span class="glyphicon glyphicon-chevron-right"></span></div><div class="carousel-control left arrow left-arrow" href="#" ng-click="prevImg()"><span class="glyphicon glyphicon-chevron-left"></span></div></div><div class="thumbs" ng-style="thumbsStyle"><div class="scroll" state="loading" ng-style="thumbsScrollStyle"><span class="box" ng-class="{firstBox:$first}" ng-repeat="image in images" index = "{{$index}}"><span class="helper"></span><img  ng-src="{{image.url}}"></span></div><div class="carousel-control right arrow right-arrow" href="#" ng-click="nextThumb()"><span class="glyphicon glyphicon-chevron-right"></span></div><div class="carousel-control left arrow left-arrow" href="#" ng-click="prevThumb()"><span class="glyphicon glyphicon-chevron-left"></span></div></div></div>',
    	link:function(scope, element, attr) {
    		var angularSlider = {
    			slider:{}
    		};

    		scope.$watch(attr.images, function(value) {		        
		        updateImages();
	      	});

			angularSlider.slider.viewer = {
		        width: 0.8469539375928678 * attr.width -1,
		        height: 0.7043650793650794 * attr.height,
		        currentPosition: 0,
		        scrollPosition: 0
		    };

		    angularSlider.slider.thumbs = {
		        width: 0.8469539375928678 * attr.width,
		        height: 0.253968253968254 * attr.height,
		        currentPosition: 0,
		        scrollPosition: 0
		    };    
		    
		    //Styles
		    
		    element.css({
		    	width:              attr.width + "px",
		        height:             attr.height + "px"
		    });
		    var slider = 		element.find(".slider"),
		    	viewer = 		slider.find(".viewer"),
		    	viewerScroll = 	viewer.find(".scroll"),
		    	thumbs = 		element.find(".thumbs"),
		    	thumbsScroll = 	thumbs.find(".scroll");

		    slider.css = ({
		        width:              attr.width + "px",
		        height:             attr.height + "px"
		    });
		    viewer.css ({
		        width: angularSlider.slider.viewer.width + "px",
		        height: angularSlider.slider.viewer.height + "px"
		    });
		    thumbs.css ({
		        width: angularSlider.slider.thumbs.width + "px",
		        height: angularSlider.slider.thumbs.height + "px"
		    });
		    updateImages = function(){
		    	var thumbsBox = 	thumbsScroll.find(".box"),
		    		viewerBox =		viewerScroll.find(".box");

		    	viewerScroll.css ({
			        width: angularSlider.slider.viewer.width * attr.images + "px",
			        height: angularSlider.slider.viewer.height + "px"
			    });
			    thumbsScroll.css ({
			        width: (angularSlider.slider.thumbs.width/4) * attr.images + "px",
			        height: angularSlider.slider.thumbs.height + "px"
			    });

			    viewerBox.css ({
			        width: angularSlider.slider.viewer.width + "px",
			        height: angularSlider.slider.viewer.height + "px"
			    });

			    thumbsBox.css ({
			        width: (angularSlider.slider.thumbs.width/4) + "px",
			        height: angularSlider.slider.thumbs.height + "px"
			    }).on("click", function(el){
			    	var index = el.currentTarget.attributes.index.value;
			        angularSlider.slider.viewer.currentPosition = index;
			        angularSlider.slider.viewer.scrollPosition =   angularSlider.slider.viewer.width      * angularSlider.slider.viewer.currentPosition;
			        angularSlider.viewerScroll(angularSlider.slider.viewer.scrollPosition * -1);
			        angularSlider.setActive(angularSlider.slider.viewer.currentPosition);
		    	});
		    };
		    element.find(".viewer .carousel-control.right").on("click", function(){
		    	var overflow = (angularSlider.slider.viewer.currentPosition + 2> attr.images);
		        
		        if( !overflow )
		            angularSlider.slider.viewer.currentPosition ++;
		        
		        angularSlider.slider.viewer.scrollPosition =   angularSlider.slider.viewer.width      * angularSlider.slider.viewer.currentPosition;
		        angularSlider.viewerScroll(angularSlider.slider.viewer.scrollPosition * -1);   
		        angularSlider.setActive(angularSlider.slider.viewer.currentPosition);
		    });		    		    
		    element.find(".viewer .carousel-control.left").on("click", function(){
		        var overflow = (angularSlider.slider.viewer.currentPosition - 1 < 0);
		        
		        if( !overflow )
		            angularSlider.slider.viewer.currentPosition --;
		            
		        angularSlider.slider.viewer.scrollPosition =   angularSlider.slider.viewer.width      * angularSlider.slider.viewer.currentPosition;
		        angularSlider.viewerScroll(angularSlider.slider.viewer.scrollPosition * -1);
		        angularSlider.setActive(angularSlider.slider.viewer.currentPosition);
		    });
		    element.find(".thumbs .carousel-control.right").on("click", function(){
		        var overflow = (angularSlider.slider.thumbs.currentPosition + 1> (attr.images/4));
		        
		        if( !overflow )
		            angularSlider.slider.thumbs.currentPosition ++;
		        
		        angularSlider.slider.thumbs.scrollPosition =   angularSlider.slider.thumbs.width      * angularSlider.slider.thumbs.currentPosition;
		        angularSlider.thumbsScroll(angularSlider.slider.thumbs.scrollPosition * -1);
		        angularSlider.setActive(angularSlider.slider.viewer.currentPosition);
		    });
		    element.find(".thumbs .carousel-control.left").on("click", function(){
		        var overflow = (angularSlider.slider.thumbs.currentPosition - 1 < 0);
		        
		        if( !overflow )
		            angularSlider.slider.thumbs.currentPosition --;
		            
		        angularSlider.slider.thumbs.scrollPosition =   angularSlider.slider.thumbs.width      * angularSlider.slider.thumbs.currentPosition;
		        angularSlider.thumbsScroll(angularSlider.slider.thumbs.scrollPosition * -1);
		        angularSlider.setActive(angularSlider.slider.viewer.currentPosition);
		    });


		    angularSlider.viewerScroll = function(position){
		        viewerScroll
		        .css("transform", "translate("+ position +"px, 0px)")
		        .css("-webkit-transform", "translate("+ position +"px, 0px)")
		        .css("-moz-transform", "translate("+ position +"px, 0px)")
		        .css("-o-transform", "translate("+ position +"px, 0px)")
		        .css("-ms-transform", "translate("+ position +"px, 0px)");
		    };
		    angularSlider.thumbsScroll = function(position){
		        thumbsScroll
		        .css("transform", "translate("+ position +"px, 0px)")
		        .css("-webkit-transform", "translate("+ position +"px, 0px)")
		        .css("-moz-transform", "translate("+ position +"px, 0px)")
		        .css("-o-transform", "translate("+ position +"px, 0px)")
		        .css("-ms-transform", "translate("+ position +"px, 0px)");
		    };    
		    angularSlider.setActive = function(index){		    	
		        element.find(".box").removeClass("active");
		        angular.element(viewer.find(".box")[index]).addClass("active");
		        angular.element(thumbs.find(".box")[index]).addClass("active");
		    }
		    angularSlider.viewerScroll(0);	
	    }
    } 
}]);


