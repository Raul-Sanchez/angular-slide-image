# Simple AngularJS slider with thumbnails.
Open index.html in chrome or firefox (ie comming soon)
## To customize slider size: ##
```
#!javascript
/app/scripts/controllers/main.js
...
12 $scope.slider = {
13     	width: 				677,
14     	height: 			505
15     };    
```
![angular-slide-images.png](https://bitbucket.org/repo/AnGR84/images/2437909821-angular-slide-images.png)